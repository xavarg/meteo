package com.afpa.meteo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;


import com.afpa.meteo.model.OpenweatherMap;
import com.afpa.meteo.model.OwWeather;
import com.afpa.meteo.service.FastDialog;
import com.afpa.meteo.service.Network;
import com.afpa.meteo.utils.Constante;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    private EditText editTextCity;
    private Button buttonSubmit;
    private TextView textViewCity;
    private TextView textViewTemperature;
    private ImageView imageViewIcon;
    List<OwWeather> weather = new ArrayList<> ();
    List<OpenweatherMap> openweatherMaps = new ArrayList<> ();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_main);

        editTextCity = (EditText) findViewById(R.id.editTextCity);
        buttonSubmit = (Button) findViewById(R.id.buttonSubmit);
        textViewCity = (TextView) findViewById(R.id.textViewCity);
        textViewTemperature = (TextView) findViewById(R.id.textViewTemperature);
        imageViewIcon = (ImageView) findViewById(R.id.imageViewIcon);

        buttonSubmit.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {

                if(!editTextCity.getText ().toString ().isEmpty ()){
                    if (Network.isNetworkAvailable (MainActivity.this)){


                        RequestQueue queue = Volley.newRequestQueue(MainActivity.this);
                        String url = String.format (Constante.URL_METEO,editTextCity.getText ().toString ());


                        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {

                                        Log.e("json","resultat"+response);

                                        Gson myGson = new Gson ();
                                        OpenweatherMap result = myGson.fromJson (response, OpenweatherMap.class);

                                        if(result.getCod ().equals ("200")){
                                        textViewCity.setText (result.getName ());
                                        textViewTemperature.setText (result.getOwMain ().getTemp ());


                                        String urlImage = String.format (Constante.URL_IMAGE,result.getWeather ().get (0).getIcon());
                                        Picasso.get ().load (urlImage).into (imageViewIcon);
                                        } else{
                                            FastDialog.showDialog (MainActivity.this,FastDialog.SIMPLE_DIALOG,result.getMessage ()
                                            );
                                        }

                                    }
                                }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                editTextCity.setText("That didn't work!");
                            }
                        });

                    }else {

                    }

                }else { // no city

                    FastDialog.showDialog (MainActivity.this, FastDialog.SIMPLE_DIALOG,
                            "Vous devez renseigner une ville");
                }

            }
        });
    }
}
