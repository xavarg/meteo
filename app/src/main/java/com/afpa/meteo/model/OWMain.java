package com.afpa.meteo.model;

public class OWMain {

    public String temp;

    public OWMain(String temp) {
        this.temp = temp;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    @Override
    public String toString() {
        return "OWMain{" +
                "temp='" + temp + '\'' +
                '}';
    }
}
