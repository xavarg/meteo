package com.afpa.meteo.model;

import java.util.List;

public class OpenweatherMap {
    public String name;
    public String cod;
    public String message;
    private OWMain owMain;

    private List<OwWeather> weather;
    // weather au singulier correspond à parametre dans le fichier Gson de l'appli Openweather;
    // ne pas mettre de "s" comme pour une List<>;


    public OpenweatherMap(String name, String cod, String message, OWMain owMain, List<OwWeather> weather) {
        this.name = name;
        this.cod = cod;
        this.message = message;
        this.owMain = owMain;
        this.weather = weather;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public OWMain getOwMain() {
        return owMain;
    }

    public void setOwMain(OWMain owMain) {
        this.owMain = owMain;
    }

    public List<OwWeather> getWeather() {
        return weather;
    }

    public void setWeather(List<OwWeather> weather) {
        this.weather = weather;
    }

    @Override
    public String toString() {
        return "OpenweatherMap{" +
                "name='" + name + '\'' +
                ", cod='" + cod + '\'' +
                ", message='" + message + '\'' +
                ", owMain=" + owMain +
                ", weather=" + weather +
                '}';
    }
}
