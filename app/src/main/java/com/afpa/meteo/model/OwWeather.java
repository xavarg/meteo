package com.afpa.meteo.model;

public class OwWeather {

    private String icon;

    public OwWeather(String icon) {
        this.icon = icon;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Override
    public String toString() {
        return "OwWeather{" +
                "icon='" + icon + '\'' +
                '}';
    }
}
