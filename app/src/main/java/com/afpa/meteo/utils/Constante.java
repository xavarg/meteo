package com.afpa.meteo.utils;

public class Constante {

    public static final String URL_METEO ="https://api.openweathermap.org/data/2.5/weather?q=%s,fr&units=metric&appid=a749c68b1d6b747844f278d71e4c718d";
    public static final String URL_IMAGE = "https://openweathermap.org/img/w/%s.png";
}
